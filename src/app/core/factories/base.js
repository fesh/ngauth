(function(angular, nga) {
	'use strict';

	/**
	 * @ngdoc factory
	 * @name nga.core.factories.BaseModel
	 * @description Базовая модель.
	 */
	angular.module(nga.core.name).factory(nga.core.factories.BaseModel,
		function() {
			function BaseModel() {}

			BaseModel.prototype.keyMap = {};
			BaseModel.prototype.serializationRules = {};

			/**
			 * @ngdoc method
			 * @name serialize
			 * @methodOf nga.core.factories.BaseModel
			 * @description Сериализация данных модели для передачи backend'у.
			 */
			BaseModel.prototype.serialize = function() {
				if (angular.isFunction(this.beforeSerialize)) {
					this.beforeSerialize();
				}

				var fields = {};

				for (var key in this.keyMap) {
					var prop = this.keyMap[key];

					if (!this.hasOwnProperty(prop)) {
						continue;
					}

					if (prop in this.serializationRules) {
						if (angular.isDefined(this.serializationRules[prop].serialize)) {
							if (angular.isFunction(this.serializationRules[prop].serialize)) {
								fields[key] = this.serializationRules[prop].serialize(this[prop], this);
							} else {
								delete fields[key];
							}
						} else {
							fields[key] = this[prop];
						}
					} else {
						fields[key] = this[prop];
					}
				}

				return fields;
			};

			/**
			 * @ngdoc method
			 * @name unserialize
			 * @methodOf nga.core.factories.BaseModel
			 * @description Десериализация данных модели от backend'а.
			 */
			BaseModel.prototype.unserialize = function(fields) {
				if (angular.isObject(fields)) {
					for (var key in this.keyMap) {
						var prop = this.keyMap[key];

						if (!fields.hasOwnProperty(key)) {
							continue;
						}

						if (prop in this.serializationRules) {
							if (angular.isDefined(this.serializationRules[prop].unserialize)) {
								if (angular.isFunction(this.serializationRules[prop].unserialize)) {
									this[prop] = this.serializationRules[prop].unserialize(fields[key], this);
								} else {
									delete this[prop];
								}
							} else {
								this[prop] = fields[key];
							}
						} else {
							this[prop] = fields[key];
						}
					}

					if (angular.isFunction(this.afterUnserialize)) {
						this.afterUnserialize();
					}
				}
			};

			/**
			 * @ngdoc method
			 * @name clone
			 * @methodOf nga.core.factories.BaseModel
			 * @description Клонирование объекта.
			 */
			BaseModel.prototype.clone = function() {
				if (!this.constructor) {
					return null;
				}
				var model = new this.constructor();
				angular.extend(model, this);
				return model;
			};

			return BaseModel;
		}
	);

	/**
	 * @ngdoc factory
	 * @name nga.core.factories.BaseManager
	 * @description Базовый менеджер, работающий с backend'ом.
	 */
	angular.module(nga.core.name).factory(nga.core.factories.BaseManager, [
		'$http',
		'$q',
		nga.core.constants.ApiConfig,

		function($http, $q, ApiConfig) {
			function BaseManager(modelType, modelName) {
				this.modelType = modelType;
				this.modelName = modelName;
				this.route = 'model/' + modelName;
			}

			/**
			 * @ngdoc method
			 * @name buildApiUrl
			 * @methodOf nga.core.factories.BaseManager
			 * @description Формирование параметров запроса к API.
			 */
			BaseManager.prototype.buildApiUrl = function(route, options) {
				var base = ApiConfig.url + '/' + route;
				if (angular.isUndefined(options)) {
					return base;
				}

				var query = [];
				if (angular.isDefined(options.filters)) {
					if (!angular.isArray(options.filters)) {
						options.filters = [options.filters];
					}
					if (options.filters.length > 0) {
						query.push('filter=' + encodeURIComponent(JSON.stringify(options.filters)));
					}
				}
				if (angular.isDefined(options.offset) && angular.isDefined(options.limit)) {
					query.push('offset=' + encodeURIComponent(options.offset));
					query.push('limit=' + encodeURIComponent(options.limit));
				}
				if (angular.isDefined(options.order)) {
					query.push('order=' + encodeURIComponent(JSON.stringify(options.order)));
				}
				if (angular.isDefined(options.count) && options.count) {
					query.push('count=true');
				}

				return query.length > 0 ? (base + (base.indexOf('?') == -1 ? '?' : '&') + query.join('&')) : base;
			};

			/**
			 * @ngdoc method
			 * @name runMethod
			 * @methodOf nga.core.factories.BaseManager
			 * @description Вызов метода контроллера.
			 */
			BaseManager.prototype.runMethod = function(name, options) {
				var route = this.route;
				var method = 'GET';
				var data = {};

				if (angular.isDefined(options)) {
					if (angular.isDefined(options.route)) {
						route = options.route;
					}
					if (angular.isDefined(options.method)) {
						method = options.method;
					}
					if (angular.isDefined(options.data)) {
						data = options.data;
					}
				}

				return $http({
					url: this.buildApiUrl(route + (name ? '/' + name : ''), options),
					method: method,
					data: data
				}).then(function(response) {
					return response.data;
				});
			};

			/**
			 * @ngdoc method
			 * @name getList
			 * @methodOf nga.core.factories.BaseManager
			 * @description Получение списка элементов.
			 */
			BaseManager.prototype.getList = function(models, options) {
				var self = this;
				var route = this.route;
				var method = 'GET';
				var data = {};

				if (angular.isDefined(options)) {
					if (angular.isDefined(options.route)) {
						route = options.route;
					}
					if (angular.isDefined(options.method)) {
						method = options.method;
					}
					if (angular.isDefined(options.data)) {
						data = options.data;
					}
				}

				var requestOptions = {
					url: this.buildApiUrl(route, options),
					method: method
				};
				if (method == 'GET') {
					requestOptions.params = data;
				} else {
					requestOptions.data = data;
				}

				return $http(requestOptions).then(function(response) {
					models.splice(0, models.length);
					angular.forEach(response.data, function(data) {
						var model = new self.modelType(data);
						models.push(model);
					});
					return models;
				});
			};

			/**
			 * @ngdoc method
			 * @name getMap
			 * @methodOf nga.core.factories.BaseManager
			 * @description Получение словаря из элементов.
			 */
			BaseManager.prototype.getMap = function(models, options) {
				var self = this;
				var route = this.route;
				var method = 'GET';
				var primaryKey = 'id';
				var data = {};

				if (angular.isDefined(options)) {
					if (angular.isDefined(options.route)) {
						route = options.route;
					}
					if (angular.isDefined(options.method)) {
						method = options.method;
					}
					if (angular.isDefined(options.data)) {
						data = options.data;
					}
					if (angular.isDefined(options.primaryKey)) {
						primaryKey = options.primaryKey;
					}
				}

				var requestOptions = {
					url: this.buildApiUrl(route, options),
					method: method
				};
				if (method == 'GET') {
					requestOptions.params = data;
				} else {
					requestOptions.data = data;
				}

				return $http(requestOptions).then(function(response) {
					for (var id in models) {
						delete models[id];
					}
					angular.forEach(response.data, function(data) {
						var model = new self.modelType(data);
						var id = model[primaryKey];
						models[id] = model;
					});
					return models;
				});
			};

			/**
			 * @ngdoc method
			 * @name getSingle
			 * @methodOf nga.core.factories.BaseManager
			 * @description Получение одного элемента по ID.
			 */
			BaseManager.prototype.getSingle = function(model, id, options) {
				var route = this.route;
				var method = 'GET';
				var data = {};

				if (angular.isDefined(options)) {
					if (angular.isDefined(options.route)) {
						route = options.route;
					}
					if (angular.isDefined(options.method)) {
						method = options.method;
					}
					if (angular.isDefined(options.data)) {
						data = options.data;
					}
				}

				var requestOptions = {
					url: this.buildApiUrl(route + '/' + id),
					method: method
				};
				if (method == 'GET') {
					requestOptions.params = data;
				} else {
					requestOptions.data = data;
				}

				return $http(requestOptions).then(function(response) {
					if (angular.isArray(response.data)) { // И принесет нам боль и страдания...
						model.unserialize(response.data[0]);
						return model;
					} else {
						model.unserialize(response.data);
						return model;
					}
				});
			};

			/**
			 * @ngdoc method
			 * @name create
			 * @methodOf nga.core.factories.BaseManager
			 * @description Создание нового элемента.
			 */
			BaseManager.prototype.create = function(model) {
				return $http({
					url: this.buildApiUrl(this.route),
					method: 'PUT',
					data: model.serialize()
				}).then(function(response) {
					model.unserialize(response.data);
					return model;
				});
			};

			/**
			 * @ngdoc method
			 * @name update
			 * @methodOf nga.core.factories.BaseManager
			 * @description Обновление данных элемента.
			 */
			BaseManager.prototype.update = function(model) {
				return $http({
					url: this.buildApiUrl(this.route) + '/' + model.id,
					method: 'PATCH',
					data: model.serialize()
				}).then(function(response) {
					model.unserialize(response.data);
					return model;
				});
			};

			/**
			 * @ngdoc method
			 * @name destroy
			 * @methodOf nga.core.factories.BaseManager
			 * @description Удаление элемента.
			 */
			BaseManager.prototype.destroy = function(model) {
				return $http({
					url: this.buildApiUrl(this.route) + '/' + model.id,
					method: 'DELETE'
				}).then(function() {
					model.deleted = true;
					model.selected = false;
					return model;
				});
			};

			/**
			 * @ngdoc method
			 * @name serializeList
			 * @methodOf nga.core.factories.BaseManager
			 * @description Сериализация массива моделей.
			 */
			BaseManager.prototype.serializeList = function(models) {
				var result = [];
				if (models && Array.isArray(models)) {
					angular.forEach(models, function(obj) {
						if (typeof obj.serialize === 'function' && obj.deleted !== true) {
							result.push(obj.serialize());
						}
					});
				}
				return result;
			};

			/**
			 * @ngdoc method
			 * @name unserializeList
			 * @methodOf nga.core.factories.BaseManager
			 * @description Десериализация массива моделей.
			 */
			BaseManager.prototype.unserializeList = function(objects) {
				var result = [];
				var self = this;
				if (objects && Array.isArray(objects)) {
					angular.forEach(objects, function(obj) {
						result.push(new self.modelType(obj));
					});
				}
				return result;
			};

			/**
			 * @ngdoc method
			 * @name listToMap
			 * @methodOf nga.core.factories.BaseManager
			 * @description Преобразование массива элементов в словарь ID=элемент.
			 */
			BaseManager.prototype.listToMap = function(models, primaryKey) {
				primaryKey = primaryKey || 'id';

				var result = {};
				angular.forEach(models, function(model) {
					result[model[primaryKey]] = model;
				});

				return result;
			};

			return BaseManager;
		}
	]);
})(angular, nga);