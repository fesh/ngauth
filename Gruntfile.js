module.exports = function(grunt) {
	/** 
	 * Загружаем списки необходимых задач для Grunt. Они устанавливаются в соответствии
	 * с версиями, указанными в `package.json`, когда вы делаете `npm install` в этой
	 * директории.
	 */
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-replace');
	grunt.loadNpmTasks('grunt-conventional-changelog');
	grunt.loadNpmTasks('grunt-bump');
	grunt.loadNpmTasks('grunt-ngdocs');
	grunt.loadNpmTasks('grunt-notify');

	/**
	 * Загружаем пользовательский файл конфигурации.
	 */
	var userConfig = require('./build.config.js');

	/**
	 * Объект конфигурации, используемый Grunt для обеспечения работы плагинов.
	 */
	var taskConfig = {
		/**
		 * Читаем файл `package.json`, чтобы иметь возможность доступа к имени
		 * пакета и его версии.
		 */
		pkg: grunt.file.readJSON('package.json'),

		/**
		 * Настройка уведомлений
		 */
		notify_hooks: {
			options: {
				enabled: true,
				max_jshint_notifications: 1,
				title: 'Шеф! Все пропало!',
				success: false,
				duration: 10
			}
		},

		/**
		 * Вешаем верхний баннер в виде комментария к исходным файлам. Обрабатываем
		 * его как шаблон Grunt, чтобы дать возможность использовать подстановки.
		 */
		meta: {
			banner: '/**\n' +
				' * <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
				' * <%= pkg.homepage %>\n' +
				' *\n' +
				' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
				' * Licensed <%= pkg.licenses.type %> <<%= pkg.licenses.url %>>\n' +
				' */\n'
		},

		/**
		 * Заполняем список изменений в новой версии.
		 */
		changelog: {
			options: {
				dest: 'CHANGELOG.md',
				template: 'changelog.tpl'
			}
		},

		/**
		 * Устанавливаем номер версии.
		 */
		bump: {
			options: {
				files: [
					'package.json',
					'bower.json'
				],
				updateConfigs: ['pkg'],
				commit: false,
				createTag: false,
				push: false
			}
		},

		/**
		 * Определяем список директорий для очистки при выполнении `grunt clean`.
		 */
		clean: {
			options: {
				force: true
			},
			stuff: [
				'<%= build_dir %>',
				'<%= compile_dir %>'
			]
		},

		/**
		 * Определяем, какие файлы необходимо скопировать из исходной папки в
		 * папку сборки. Ресурсы (картинки, шрифты и т.п.), а также javascript-код
		 * помещаем в папку `build_dir`, а затем копируем ресурсы в папку `compile_dir`.
		 */
		copy: {
			build_common_files: {
				files: [{
					src: ['<%= common_files.js %>'],
					dest: '<%= build_dir %>/',
					cwd: '.',
					expand: true
				}]
			},
			build_app_assets: {
				files: [{
					src: ['**'],
					dest: '<%= build_dir %>/assets/',
					cwd: 'src/assets',
					expand: true
				}]
			},
			build_vendor_assets: {
				files: [{
					src: ['<%= vendor_files.assets %>'],
					dest: '<%= build_dir %>/assets/',
					cwd: '.',
					expand: true
				}]
			},
			build_appjs: {
				files: [{
					src: ['<%= app_files.js %>'],
					dest: '<%= build_dir %>/',
					cwd: '.',
					expand: true
				}]
			},
			build_vendorjs: {
				files: [{
					src: ['<%= vendor_files.js %>', '<%= test_files.js %>'],
					dest: '<%= build_dir %>/',
					cwd: '.',
					expand: true
				}]
			},
			build_vendorcss: {
				files: [{
					src: ['<%= vendor_files.css %>'],
					dest: '<%= build_dir %>/',
					cwd: '.',
					expand: true
				}]
			},
			build_vendorfonts: {
				files: [{
					src: ['<%= vendor_files.fonts %>'],
					dest: '<%= build_dir %>/assets/fonts/',
					cwd: '.',
					expand: true,
					flatten: true
				}]
			},
			compile_common_files: {
				files: [{
					src: ['<%= common_files.js %>'],
					dest: '<%= compile_dir %>/',
					cwd: '.',
					expand: true
				}]
			},
			compile_assets: {
				files: [{
					src: ['**'],
					dest: '<%= compile_dir %>/assets',
					cwd: '<%= build_dir %>/assets',
					expand: true
				}, {
					src: ['<%= vendor_files.css %>'],
					dest: '<%= compile_dir %>/assets/fonts/',
					cwd: '.',
					expand: true,
					flatten: true
				}]
			},
			compile_vendorfonts: {
				files: [{
					src: ['<%= vendor_files.fonts %>'],
					dest: '<%= compile_dir %>/',
					cwd: '.',
					expand: true
				}]
			}
		},

		/**
		 * После склеивания всех CSS файлов (проект+вендор)
		 * нужно результат минифицировать
		 */
		cssmin: {
			compile: {
				options: {
					banner: '<%= meta.banner %>'
				},
				files: {
					'<%= compile_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>.css': '<%= compile_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>.css'
				}
			}
		},

		/**
		 * Составляем список для автозамены, который используем, например, для
		 * подстановки версии пакета к URL файлов шаблонов, чтобы предотвратить
		 * загрузку старых шаблонов из кеша браузера.
		 */
		replace: {
			modify_template_urls: {
				options: {
					patterns: [{
						match: /(\.tpl\.html)/ig,
						replacement: '$1?v=<%= pkg.version %>'
					}],
					usePrefix: false
				},
				files: [{
					src: 'src/**/*.js',
					dest: '<%= build_dir %>',
					cwd: '<%= build_dir %>',
					expand: true
				}]
			},
			fix_fontawesome_css: {
				options: {
					patterns: [{
						match: /\.\.\/fonts\//g,
						replacement: 'fonts/'
					}],
					usePrefix: false
				},
				files: [{
					src: 'assets/<%= pkg.name %>-<%= pkg.version %>.css',
					dest: '<%= build_dir %>',
					cwd: '<%= build_dir %>',
					expand: true
				}, {
					src: 'assets/<%= pkg.name %>-<%= pkg.version %>.css',
					dest: '<%= compile_dir %>',
					cwd: '<%= compile_dir %>',
					expand: true
				}]
			}
		},

		/**
		 * Определяем, какие файлы следует объединить в единый файл при выполнении
		 * команды `grunt concat`.
		 */
		concat: {
			/**
			 * Цель `build_css` объединяет наши скомпилированные CSS файлы, а также
			 * CSS файлы из сторонних пакетов.
			 */
			build_css: {
				src: [
					'<%= vendor_files.css %>',
					'<%= build_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>.css'
				],
				dest: '<%= build_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>.css'
			},

			/**
			 * Цель `compile_css` объединяет главный скомпилированный CSS файл с CSS
			 * файлами сторонних пакетов.
			 */
			compile_css: {
				src: [
					'<%= vendor_files.css %>',
					'<%= compile_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>.css'
				],
				dest: '<%= compile_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>.css'
			},

			/**
			 * Цель `compile_js` объединяет наш исходный код скриптов, а также JS-код
			 * из сторонних пакетов.
			 */
			compile_js: {
				options: {
					banner: '<%= meta.banner %>',
					sourceMap: true
				},
				src: [
					'<%= vendor_files.js %>',
					'module.prefix',
					'<%= build_dir %>/src/app/ngauth.js',
					'<%= build_dir %>/src/app/core.js',
					'<%= build_dir %>/src/app/app.js',
					'<%= build_dir %>/src/**/*.js',
					'module.suffix'
				],
				dest: '<%= compile_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>.js'
			}
		},

		/**
		 * Минифицируем исходные файлы скриптов.
		 */
		uglify: {
			compile: {
				options: {
					banner: '<%= meta.banner %>',
					sourceMap: true,
					sourceMapIncludeSources: true,
					sourceMapIn: '<%= compile_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>.js.map'
					//mangle: false
				},
				files: {
					'<%= concat.compile_js.dest %>': '<%= concat.compile_js.dest %>'
				}
			}
		},

		/**
		 * `grunt-contrib-less` занимается компиляцией LESS и их минификацией.
		 * `main.less` - единственный файл, который включен в компиляцию, все остальные
		 * файлы должны импортироваться из него.
		 */
		less: {
			build: {
				files: {
					'<%= build_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>.css': '<%= app_files.less %>'
				}
			},
			compile: {
				files: {
					'<%= compile_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>.css': '<%= app_files.less %>'
				},
				options: {
					cleancss: true,
					compress: true
				}
			}
		},

		/**
		 * `jshint` формирует список файлов, которые мы должны проверить. Исходные
		 * файлы скриптов (включая, файлы юнит-тестирования) проверяются на основе
		 * правил, определенных в секции `options`.
		 */
		jshint: {
			src: [
				'<%= common_files.js %>',
				'<%= app_files.js %>'
			],
			test: [
				'<%= app_files.jsunit %>'
			],
			gruntfile: [
				'Gruntfile.js'
			],
			options: {
				curly: true,
				immed: true,
				newcap: true,
				noarg: true,
				sub: true,
				boss: true,
				eqnull: true
			},
			globals: {}
		},

		/**
		 * Настройки сборщика документации.
		 */
		ngdocs: {
			all: ['src/**/*.js']
		},

		/**
		 * Задача `index` занимается компиляцией файла `index.html` как шаблона
		 * Grunt.
		 */
		index: {
			/**
			 * Во время разработки мы не хотим дожидаться выполнения всех этапов
			 * сборки (объединения, минификации и т.п.), поэтому мы просто добавляем
			 * все скриптовые файлы в `<head>` секцию `index-html`. В свойстве `src`
			 * будет содержаться список включаемых файлов.
			 */
			build: {
				dir: '<%= build_dir %>',
				src: [
					'<%= vendor_files.js %>',
					'<%= build_dir %>/src/app/ngauth.js',
					'<%= build_dir %>/src/app/core.js',
					'<%= build_dir %>/src/app/app.js',
					'<%= build_dir %>/src/**/*.js',
					'<%= build_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>.css'
				]
			},

			/**
			 * Во время финальной сборки мы объединяем скриптовые файлы, а также
			 * CSS файлы.
			 */
			compile: {
				dir: '<%= compile_dir %>',
				src: [
					'<%= concat.compile_js.dest %>',
					'<%= build_dir %>/assets/<%= pkg.name %>-<%= pkg.version %>.css'
				]
			}
		},

		/**
		 * Для поддержания быстрой разработки мы наблюдаем за измененными файлами
		 * и запускаем соответствующие задачи. Это избавляет нас от необходимости
		 * ручного запуска `grunt` каждый раз, когда требуется увидеть сделанные
		 * изменения; мы просто единожды запускаем `grunt watch` как фоновую задачу,
		 * а дальше он сам следит за пересборкой.
		 */
		delta: {
			/**
			 * По умолчанию, мы хотим, чтобы Live Reload работал со всеми задачами.
			 * Это поведение может быть изменено в отдельных задачах, которые не
			 * трогают ресурсы, используемые браузером. По умолчанию он висит на
			 * порту 35729, который плагин браузера должен обнаружить автоматически.
			 */
			options: {
				livereload: true
			},

			/**
			 * При изменении файла `Gruntfile.js` мы должны проверить его, при этом
			 * он будет автоматически перезагружен.
			 */
			gruntfile: {
				files: 'Gruntfile.js',
				tasks: ['jshint:gruntfile'],
				options: {
					livereload: false
				}
			},

			/**
			 * При изменениях скриптов мы также проверяем их, а также запускаем
			 * юнит-тесты.
			 */
			jssrc: {
				files: [
					'<%= common_files.js %>',
					'<%= app_files.js %>'
				],
				tasks: ['jshint:src', 'copy:build_common_files', 'copy:build_appjs']
			},

			/**
			 * После изменения ресурсов, вновь копируем их в целевые папки.
			 * Имейте в виду, что новые файлы скопированы не будут, поэтому
			 * данная задача не так уж полезна.
			 */
			assets: {
				files: [
					'src/assets/**/*'
				],
				tasks: ['copy:build_app_assets', 'copy:build_vendor_assets']
			},

			/**
			 * Перекомпилируем `index.html` при изменениях.
			 */
			html: {
				files: ['<%= app_files.html %>'],
				tasks: ['index:build']
			},

			/**
			 * При изменениях в CSS файлах, мы перекомпилируем и минифицируем их.
			 */
			less: {
				files: ['src/**/*.less'],
				tasks: ['less:build', 'concat:build_css', 'replace:fix_fontawesome_css']
			},

			/**
			 * Когда изменяются скриптовые файлы юнит-тестирования, мы проверяем их
			 * и запускаем тесты, при этом нам не требуется перезагрузка.
			 */
			jsunit: {
				files: [
					'<%= app_files.jsunit %>'
				],
				tasks: ['jshint:test'],
				options: {
					livereload: false
				}
			}
		}
	};

	grunt.initConfig(grunt.util._.extend(taskConfig, userConfig));

	/**
	 * Чтобы компилировать и копировать только измененные файлы, мы должны
	 * удостовериться, что начинаем с чистой, свежей сборки. Поэтому мы
	 * переименовываем задачу `watch` в `delta` (именно поэтому переменная
	 * выше называется `delta`), а также добавляем новую задачу `watch`,
	 * которая очищает сборку перед началом наблюдения за изменениями.
	 */
	grunt.renameTask('watch', 'delta');
	grunt.registerTask('watch', ['build', 'delta']);

	/**
	 * Задача по умолчанию включает в себя сборку и компиляцию.
	 */
	grunt.registerTask('default', ['build', 'compile']);

	/**
	 * Задача `build` подготавливает приложение к разработке и тестированию.
	 */
	grunt.registerTask('build', [
		'clean', 'jshint',
		'less:build',
		'copy:build_common_files',
		'copy:build_app_assets', 'copy:build_vendor_assets', 'copy:build_vendorfonts',
		'copy:build_appjs', 'replace:modify_template_urls', 'copy:build_vendorjs',
		'copy:build_vendorcss', 'concat:build_css', 'replace:fix_fontawesome_css',
		'index:build'
	]);

	/**
	 * Задача `compile` подготавливает приложение к релизу, объединяя файлы и
	 * минифицируя их.
	 */
	grunt.registerTask('compile', [
		'less:compile',
		'copy:compile_common_files',
		'copy:compile_assets', 'copy:compile_vendorfonts',
		'concat:compile_js', 'uglify',
		'concat:compile_css', 'replace:fix_fontawesome_css',
		'index:compile',
		'cssmin:compile'
	]);

	/**
	 * Задача `docs` собирает документацию по исходным файлам.
	 */
	grunt.registerTask('docs', ['ngdocs:all']);

	/**
	 * Вспомогательная функция для получения списка JS-файлов.
	 */
	function filterForJS(files) {
		return files.filter(function(file) {
			return file.match(/(\.js|\.js\?.*)$/);
		});
	}

	/**
	 * Вспомогательная функция для получения списка CSS-файлов.
	 */
	function filterForCSS(files) {
		return files.filter(function(file) {
			return file.match(/\.css$/);
		});
	}

	/** 
	 * Шаблон `index.html` включает файлы стилей и скриптов, которые генерируются
	 * динамически. Эта задача преобразует списки файлов в переменные шаблона,
	 * а затем запускает компиляцию.
	 */
	grunt.registerMultiTask('index', 'Process index.html template', function() {
		var dirRE = new RegExp('^(' + grunt.config('build_dir') + '|' + grunt.config('compile_dir') + ')\/', 'g');

		var jsIncludes = grunt.config('app_files').jsinc || [];
		var jsFiles = filterForJS(this.filesSrc).map(function(file) {
			return file.replace(dirRE, '');
		});
		var cssFiles = filterForCSS(this.filesSrc).map(function(file) {
			return file.replace(dirRE, '');
		});

		grunt.file.copy('src/index.html', this.data.dir + '/index.html', {
			process: function(contents, path) {
				return grunt.template.process(contents, {
					data: {
						scripts: jsIncludes.concat(jsFiles).map(function(file) {
							if (file.indexOf('?') != -1) {
								return file + '&v=' + grunt.config('pkg.version');
							} else {
								return file + '?v=' + grunt.config('pkg.version');
							}
						}),
						styles: cssFiles,
						version: grunt.config('pkg.version')
					}
				});
			}
		});
	});

	/**
	 * Чтобы избежать ручного указания файлов, необходимых Karma для запуска,
	 * мы используем Grunt, чтобы составить список таких файлов для нас. Файлы
	 * `karma/*` компилируются как шаблоны Grunt для последующего использования
	 * в Karma.
	 */
	grunt.registerMultiTask('karmaconfig', 'Process karma config templates', function() {
		var jsFiles = filterForJS(this.filesSrc);

		grunt.file.copy('karma/karma-unit.tpl.js', grunt.config('build_dir') + '/karma-unit.js', {
			process: function(contents, path) {
				return grunt.template.process(contents, {
					data: {
						scripts: jsFiles
					}
				});
			}
		});
	});

	grunt.task.run('notify_hooks');
};