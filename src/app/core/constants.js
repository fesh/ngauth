(function(angular, nga) {
	'use strict';

	/**
	 * @ngdoc service
	 * @name nga.core.constants.ApiConfig
	 * @description Параметры API.
	 */
	angular.module(nga.core.name).constant(nga.core.constants.ApiConfig, {
		url: 'http://mir-ndv.ru/',
		csrfTokenHeaderKey: 'X-CSRF-Token'
	});
})(angular, nga);