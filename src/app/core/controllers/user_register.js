(function(angular, nga) {
	'use strict';

	/**
	 * @ngdoc controller
	 * @name nga.core.controllers.UserRegister
	 * @description Контроллер регистрации пользователя.
	 */
	angular.module(nga.core.name).controller(nga.core.controllers.UserRegister, [
		'$rootScope',
		'$scope',
		'$state',
		'toastr',
		nga.core.factories.UserManager,

		function($rootScope, $scope, $state, toastr, UserManager) {
			$scope.vars = {
				user: {},
				registerForm: {},
				captcha: null
			};

			/**
			 * @ngdoc method
			 * @name getCaptcha
			 * @methodOf nga.core.controllers.UserRegister
			 * @description Обновление капчи.
			 */
			$scope.getCaptcha = function (){
				UserManager.captcha().then(function(response){
					if (angular.isDefined(response.data.answer)) {
						$scope.vars.captcha = 'data:image/gif;base64,' + response.data.answer;
					}
				});
			};

			/**
			 * @ngdoc method
			 * @name register
			 * @methodOf nga.core.controllers.UserRegister
			 * @description Передача менеджеру пользователей запроса на регистрацию.
			 */
			$scope.register = function() {

				// валидация формы на клиенте
				$scope.$broadcast(nga.core.events.ShowFormErrorCheckValidity, 'vars.registerForm');
				if (!$scope.vars.registerForm.$valid) {
					return;
				}

				UserManager.register($scope.vars.user).then(function(data) {
					
					// одна ошибка в объекте
					if (angular.isObject(data.answer) &&
						angular.isObject(data.answer.error) &&
						angular.isDefined(data.answer.error.msg)) {

						var element = angular.element('input[name="' + data.answer.error.field + '"]');
						if (element) {
							$scope.$broadcast(nga.core.events.ShowFormErrorKickMessage, [{
								title: element.attr('placeholder'),
								message: data.answer.error.msg,
								callback: [
									function(){
										element.focus();
									},
									null
								]
							}]);
						} else {
							$scope.$broadcast(nga.core.events.ShowFormErrorKickMessage, [{
								title: data.answer.error.field,
								message: data.answer.error.msg
							}]);
						}

						return;
					}

					toastr.success('Вы успешно зарегистрированы');
					$scope.vars.user = {};
					$scope.getCaptcha();
				});
			};

			// начальная загрузка капчи
			$scope.getCaptcha();
		}
	]);
})(angular, nga);