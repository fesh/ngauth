(function(angular, nga) {
	'use strict';

	angular.module(nga.core.name).config([
		'$stateProvider',

		function($stateProvider) {
			var loginResolver = [
				'$q',
				'toastr',
				nga.core.factories.UserManager,

				function($q, toastr, UserManager) {
					var defer = $q.defer();
					UserManager.checkSession().then(function() {
						defer.resolve();
					}, function() {
						defer.resolve();
					});
					return defer.promise;
				}
			];

			$stateProvider
				.state(nga.core.states.Root, {
					url: nga.core.stateUrls.Root,
					resolve: {
						checkLogin: loginResolver
					},
					abstract: true,
					template: '<ui-view/>'
				})
				.state(nga.core.states.User, {
					parent: nga.core.states.Root,
					url: nga.core.stateUrls.User,
					abstract: true,
					template: '<ui-view/>'
				})
				.state(nga.core.states.UserRegister, {
					parent: nga.core.states.User,
					url: nga.core.stateUrls.UserRegister,
					controller: nga.core.controllers.UserRegister,
					templateUrl: 'assets/templates/user/register.tpl.html',
					data: {
						pageTitle: 'Регистрация'
					},
					breadcrumb: 'Регистрация'
				})
				.state(nga.core.states.UserAccessDenied, {
					parent: nga.core.states.User,
					url: nga.core.stateUrls.UserAccessDenied,
					templateUrl: 'assets/templates/user/noaccess.tpl.html',
					data: {
						pageTitle: 'Нет доступа'
					},
					breadcrumb: 'Нет доступа'
				})
				.state(nga.core.states.Main, {
					parent: nga.core.states.Root,
					url: nga.core.stateUrls.Main,
					controller: nga.core.controllers.Main,
					templateUrl: 'assets/templates/main.tpl.html',
					abstract: true,
					access: {
						loginRequired: true
					}
				})
				.state(nga.core.states.Dashboard, {
					parent: nga.core.states.Main,
					url: nga.core.stateUrls.Dashboard,
					controller: nga.core.controllers.Dashboard,
					templateUrl: 'assets/templates/dashboard.tpl.html',
					access: {
						loginRequired: true
					},
					data: {
						pageTitle: 'Уведомления'
					},
					breadcrumb: 'Уведомления'
				});
		}
	]);
})(angular, nga);