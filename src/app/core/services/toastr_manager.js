(function(angular, nga) {
	'use strict';

	/* global CKEDITOR_ADDONS_PATH:false, MATHJAX_SCRIPT_PATH: false */

	/**
	 * @ngdoc service
	 * @name nga.core.services.ToastrManager
	 * @description Сервис-обертка для тостера для вывода директив в тостах.
	 */
	angular.module(nga.core.name).service(nga.core.services.ToastrManager, [
		'toastr',
		'$compile',

		function ToastrManager(toastr, $compile) {

			// создание тоста
			function show($scope, type, message, title, options) {
				var toast = null;

				if (!options) {
					options = {};
				}

				options.allowHtml = true;
				options.onShown = function() {
					if (!toast || !toast.el) {
						return;
					}
					$compile(toast.el.find('div.toast-message').contents())($scope);
				};

				toast = toastr[type](message, title, options);
			}

			// обертка для создания тоста info
			this.info = function($scope, message, title, options) {
				show($scope, 'info', message, title, options);
			};

			// обертка для создания тоста error
			this.error = function($scope, message, title, options) {
				show($scope, 'error', message, title, options);
			};

			// обертка для создания тоста warning
			this.warning = function($scope, message, title, options) {
				show($scope, 'warning', message, title, options);
			};

			// обертка для создания тоста success
			this.success = function($scope, message, title, options) {
				show($scope, 'success', message, title, options);
			};
		}
	]);
})(angular, nga);