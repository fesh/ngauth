(function(angular, nga) {
	'use strict';

	angular.module(nga.core.name).config([
		'$provide',
		'$stateProvider',
		'$urlRouterProvider',
		'$httpProvider',
		'breadcrumbsProvider',
		'toastrConfig',
		'$tooltipProvider',

		function($provide, 
			$stateProvider, 
			$urlRouterProvider, 
			$httpProvider, 
			breadcrumbsProvider, 
			toastrConfig, 
			$tooltipProvider) {

			// Важно использовать именно расширенную форму вызова данного метода через передачу функцию в
			// качестве аргумента. Это решение в купе с инъекцией состояния предотвращает бесконечное
			// перенаправление с home на login и обратно.
			$urlRouterProvider.otherwise(function($injector) {
				var $state = $injector.get('$state');
				$state.go(nga.core.states.Dashboard);
			});

			// Вешаем обработчик HTTP-запросов.
			$httpProvider.interceptors.push(nga.core.interceptors.HttpHandler);

			// Настраиваем всплывающие подсказки.
			$tooltipProvider.setTriggers({
				'mouseenter': 'mouseleave',
				'click': 'click',
				'focus': 'blur',
				'never': 'mouseleave'
			});

			// Настраиваем всплывающие уведомления.
			toastrConfig.timeOut = 2000;
			toastrConfig.closeButton = true;
			toastrConfig.maxOpened = 5;
			toastrConfig.positionClass = 'toast-bottom-right';
		}
	]);
})(angular, nga);