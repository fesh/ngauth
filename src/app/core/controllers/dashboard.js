(function(angular, nga) {
	'use strict';

	/**
	 * @ngdoc controller
	 * @name nga.core.controllers.Dashboard
	 * @description Контроллер страницы уведомлений.
	 */
	angular.module(nga.core.name).controller(nga.core.controllers.Dashboard, [
		'$state',
		'$scope',
		function($state, $scope) {

		}
	]);
})(angular, nga);