(function(g) {
	'use strict';

	/* jshint unused:false */

	var pathName = '/' + location.pathname.replace(/^\/+|\/+$/gm, '') + '/';

	// Базовый URL сайта.
	g.SITE_BASE_URL = location.protocol + '//' + location.host + (location.port ? ':' + location.port : '') + pathName;

	// Путь к ресурсам.
	g.SITE_ASSETS_PATH = pathName + 'assets/';
	g.SITE_ASSETS_URL = g.SITE_BASE_URL + 'assets/';
	g.SITE_ASSETS_VENDOR_PATH = g.SITE_ASSETS_PATH + 'vendor/';
	g.SITE_ASSETS_VENDOR_URL = g.SITE_ASSETS_URL + 'vendor/';

	// Путь к сторонним библиотекам.
	g.SITE_VENDOR_PATH = pathName + 'vendor/';
	g.SITE_VENDOR_URL = g.SITE_BASE_URL + 'vendor/';
})(window);