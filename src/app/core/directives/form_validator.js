(function(angular, nga) {
	'use strict';

	/**
	 * @ngdoc directive
	 * @name nga.core.directives.FormValidator
	 * @description Валидатор формочки.
	 */
	angular.module(nga.core.name).directive(nga.core.directives.FormValidator, [
		'$compile',
		'$timeout',

		function($compile,$timeout) {
			return {
				restrict: 'A',
				require: '^form',

				scope: true,

				link: function($scope, element, attrs, formCtrl) {
					var bodyElement = angular.element(document.body);
					var container = null;
					var callbacks = [];

					function closeDialog() {
						callbacks.splice(0, callbacks.length);

						// вычищаю предыдущие алерты
						if (container && container.length > 0) {
							container.remove();
							container = null;
						}
					}

					function focusInput(name) {
						var input = bodyElement.find('[name="' + name + '"]:eq(0)');
						if (input.length > 0) {
							input.trigger('focus');
							if (input.is('select') || input.is('div')) {
								var event = document.createEvent('MouseEvents');
								event.initMouseEvent('mousedown', true, true, window);
								input[0].dispatchEvent(event);
							}
							if (input.is('div')) {
								var tagWithAction = input.find("a");
								$timeout(function() {
									tagWithAction.triggerHandler('click');
								});
							}
						}
					}

					function appendErrorMessage(message, title, callback, headingText) {
						if (!container) {
							bodyElement.append(
								'<div id="form-validator" class="alert alert-danger nga-form-validator shake">' +
								'	<h3>' + (headingText ? headingText : 'Ошибки ввода') +
								'		<div class="pull-right">' +
								'			<a class="close" href><i class="fa fa-remove"></i></a>' +
								'		</div>' +
								'	</h3>' +
								'	<div class="scroll-vertical m-t-xs" id="form_validation_message"></div>' +
								'</div>'
							);
							container = bodyElement.find('#form-validator');
							container.find('.close').on('click', closeDialog);
						}

						var messages = container.find('#form_validation_message');
						if (callback) {
							callbacks.push(callback);
							messages.append('<div class="p-b-xs"><a href ng-click="runCallback(' + (callbacks.length - 1) + ')"><i class="fa fa-search"></i> <strong>' + title + '</strong></a>: ' + (message ? message : '') + '</div>');
						} else {
							messages.append('<div class="p-b-xs"><a href><i class="fa fa-search"></i> <strong>' + title + '</strong></a>: ' + (message ? message : '') + '</div>');
						}

						return messages;
					}

					$scope.runCallback = function(index) {

						if (index < callbacks.length) {
							if (angular.isFunction(callbacks[index])) {
								callbacks[index]();
							} else if (angular.isArray(callbacks[index]) && callbacks[index].length > 0 && angular.isFunction(callbacks[index][0])) {
								callbacks[index][0].apply(this, callbacks[index].slice(1));
							}
						}
					};

					$scope.$on(nga.core.events.ShowFormErrorCloseDialog, function() {
						closeDialog();
					});

					$scope.$on(nga.core.events.ShowFormErrorCheckValidity, function(event, formName) {
						if (formName !== formCtrl.$name) {
							return;
						}

						closeDialog();

						var alerts = null;
						angular.forEach(formCtrl, function(val) {
							if (!val || val.$valid || !val.$error || angular.isUndefined(val.$error)) {
								return;
							}

							var message = '';
							if (val.$error.required) {
								message = 'Необходимо заполнить';
							}
							if (val.$error.pattern) {
								message = 'Неверный формат';
							}
							if (val.$error.date && val.$error.parse) {
								message = 'Неверный формат даты';
							}
							alerts = appendErrorMessage(
								message, (val.$options && val.$options.title) || val.$name, [focusInput, val.$name]
							);
						});
						if (container && alerts) {
							$compile(container)($scope);
						}
					});

					$scope.$on(nga.core.events.ShowFormErrorKickMessage, function(event, messages) {
						if (!angular.isArray(messages)) {
							messages = [messages];
						}

						closeDialog();

						var alerts = null;
						angular.forEach(messages, function(message) {
							alerts = appendErrorMessage(
								message.message,
								message.title,
								message.callback,
								message.headingText
							);
						});
						if (container && alerts) {
							$compile(container)($scope);
						}
					});
				}
			};
		}
	]);
})(angular, nga);