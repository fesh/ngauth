(function(angular, nga) {
	'use strict';

	/**
	 * @ngdoc service
	 * @name nga.core.services.HttpHelper
	 * @description Вспомогательные функции для работы с HTTP запросами.
	 */
	angular.module(nga.core.name).service(nga.core.services.HttpHelper, [

		function HttpHelper() {
			/**
			 * @ngdoc method
			 * @name transformObjectToQuery
			 * @methodOf nga.core.services.HttpHelper
			 * @description Преобразование объекта в строку запроса.
			 */
			this.transformObjectToQuery = function(obj, headersGetter) {
				var headers = headersGetter();
				headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				var str = [];
				for (var p in obj) {
					str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
				}

				return str.join('&');
			};
		}
	]);
})(angular, nga);