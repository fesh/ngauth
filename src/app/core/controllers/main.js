(function(angular, nga) {
	'use strict';

	/**
	 * @ngdoc controller
	 * @name nga.core.controllers.Main
	 * @description Главный контроллер.
	 */
	angular.module(nga.core.name).controller(nga.core.controllers.Main, [
		'$rootScope',
		'$scope',
		'$state',
		'$stateParams',
		nga.core.factories.UserManager,

		function($rootScope, $scope, $state, $stateParams, UserManager) {

			$scope.isMenuItemActive = function(stateName) {
				return $state.includes(stateName);
			};

			$scope.hasRole=function(role)
			{
				return UserManager.currentUser.hasRole(role);
			};

			$rootScope.reload = function(state) {
				if (!angular.isDefined(state)) {
					state = $state.current;
				}
				return $state.transitionTo(state, $stateParams, {
					reload: true
				});
			};
		}
	]);
})(angular, nga);