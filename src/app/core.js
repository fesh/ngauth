(function(angular, nga) {
	'use strict';

	nga.core = {
		name: 'Core',
		factories: {
			BaseModel: 'BaseModel',
			BaseManager: 'BaseManager',
			User: 'User',
			UserRole: 'UserRole',
			UserManager: 'UserManager'
		},
		services: {
			Common: 'Common',
			HttpHelper: 'HttpHelper',
			ToastrManager: 'ToastrManager'
		},
		directives: {
			FormValidator: 'ngaFormValidator'
		},
		interceptors: {
			HttpHandler: 'HttpHandler'
		},
		controllers: {
			UserRegister: 'UserRegisterCtrl',
			Main: 'MainCtrl',
			Dashboard: 'DashboardCtrl'
		},
		filters: {
			FormValidator: 'ngaFormValidator'
		},
		constants: {
			ApiConfig: 'ApiConfig'
		},
		enums: {
			// Пользовательские роли.
			UserRoles: {
				Support: 'support', // технический сотрудник
				SchoolUser: 'user' // обычный пользователь
			},
			// Список статусов.
			UserStatus: {
				LoginRequired: 'LoginRequired', // требуется вход
				Authorized: 'Authorized', // авторизован (имеет права доступа)
				Unauthorized: 'Unauthorized' // не авторизован (отсутствуют какие-либо права доступа)
			},
			// Список типов проверки доступа.
			AccessCheckTypes: {
				AtLeastOne: 'AtLeastOne', // как минимум одна роль требуется
				AllRequired: 'AllRequired' // требуются все роли
			},
			// Список полов.
			SexTypes: {
				Male: 'M',
				Female: 'F'
			}
		},
		events: {
			ShowFormErrorReset: 'showFormErrorReset',
			ShowFormErrorCloseDialog: 'showFormErrorCloseDialog',
			ShowFormErrorCheckValidity: 'showFormErrorCheckValidity',
			ShowFormErrorKickMessage: 'showFormErrorKickMessage'
		},
		states: {
			Root: 'root',
			User: 'user',
			Main: 'main',
			UserRegister: 'user.register',
			UserAccessDenied: 'user.accessdenied',
			Home: 'home',
			Dashboard: 'dashboard'
		},
		stateUrls: {
			Root: '',
			User: '/user',
			UserRegister: '/register',
			UserAccessDenied: '/access-denied',
			Main: '',
			Home: '/home',
			Dashboard: '/dashboard'
		}
	};

	angular.module(nga.core.name, [
		'ui.bootstrap',
		'ui.router',
		'ui.router.breadcrumbs',
		'ngAnimate',
		'toastr',
		'angular-loading-bar',
		'angularMoment'
	]);
})(angular, nga);