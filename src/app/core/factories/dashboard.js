(function(angular, nga) {
	'use strict';
	angular.module(nga.core.name).factory(nga.core.factories.Dashboard, [
		nga.core.factories.BaseModel,
		nga.core.services.Common,

		function(BaseModel, Common) {
			function Dashboard(fields) {
				BaseModel.call(this);
				this.unserialize(fields);
				this.selected = false;
				this.deleted = false;
			}
			Dashboard.prototype = new BaseModel();
			Dashboard.prototype.keyMap = {
				'lessons': 'lessons',
				'marks': 'marks',
				'mark_chart': 'markChart',
				'notes': 'notes',
				'student': 'student',
				'teacher': 'teacher',
				'parent': 'parent',
				'time': 'time'
			};
			Dashboard.prototype.serializationRules = {
				lessons: {
					unserialize: function(value) {
						angular.forEach(value, function(users) {
							angular.forEach(users, function(roles) {
								angular.forEach(roles, function(lesson) {
									lesson.start_time = Common.fromDbDateTime(lesson.start_time);
									lesson.end_time = Common.fromDbDateTime(lesson.end_time);
								});
							});
						});
						return value;
					}
				},
				marks: {
					unserialize: function(value) {
						angular.forEach(value, function(users) {
							angular.forEach(users, function(mark) {
								mark.date = Common.fromDbDateTime(mark.date);
							});
						});
						return value;
					}
				},
				teacher: {
					unserialize: function(value) {
						angular.forEach(value.unfilled_lessons, function(lesson) {
							lesson.start_time = Common.fromDbDateTime(lesson.start_time);
							lesson.end_time = Common.fromDbDateTime(lesson.end_time);
						});
						angular.forEach(value.unverified_documents, function(material) {
							material.created = Common.fromDbDateTime(material.created);
						});
						return value;
					}
				},
				time: {
					unserialize: function(value) {

						value = Common.fromDbDateTime(value);
						return value;
					}
				}
			};
			return Dashboard;
		}
	]);
	angular.module(nga.core.name).factory(nga.core.factories.DashboardManager, [
		'$q',
		'$http',
		nga.core.factories.BaseManager,
		nga.core.factories.Dashboard,
		function($q, $http, BaseManager, Dashboard) {
			function DashboardManager() {
				BaseManager.call(this, Dashboard, 'Dashboard');
			}

			DashboardManager.prototype = new BaseManager();

			/**
			 * @ngdoc method
			 * @name getDash
			 * @methodOf nga.school.factories.LessonManager
			 * @description Получение уведомлений пользователя.
			 */
			DashboardManager.prototype.getDash = function() {
				var promises = [];
				var deferred = $q.defer();
				var dashes = [];
				dashes.marks = [];
				dashes.lessons = [];
				dashes.notes = [];
				dashes.markChart = [];
				//Загружаю уведомления
				$http.get('/api/school/dashboard/get-dash').then(function(data) {
					dashes = new Dashboard(data.data);
					$q.all(promises).then(function() {
						deferred.resolve(dashes);
					});
				});
				return deferred.promise;
			};
			return new DashboardManager();
		}
	]);
})(angular, nga);