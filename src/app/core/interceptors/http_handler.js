(function(angular, nga) {
	'use strict';

	/**
	 * @ngdoc factory
	 * @name nga.core.interceptors.HttpHandler
	 * @description Обработчик HTTP-запросов. Ловит ошибки, модифицирует запрос.
	 */
	angular.module(nga.core.name).factory(nga.core.interceptors.HttpHandler, [
		'$injector',
		'$q',

		function($injector, $q) {

			// преобразование запроса JSON в стиль Form-data
			var transformRequest = function(obj, headersGetter) {
				if (!angular.isObject(obj)) {
					return '';
				}
				var headers = headersGetter();
				headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
				var str = [];
				for (var p in obj) {
					str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
				}
				return str.join('&');
			};

			return {
				'request': function(config) {
					config.transformRequest = transformRequest;

					return config || $q.when(config);
				},
				'requestError': function(request) {
					return $q.reject(request);
				},
				'response': function(response) {					
					// запрос на backend сервера
					if(angular.isObject(response.config.data)) {
						
						// если вернулся не JSON, значит ошибка
						if (!angular.isObject(response.data)) {
							
							// определяю HTTP код
							var match = response.data.match(/\d+/);
							var httpCode = (match.length > 0) ? match[0] : 'неизвестный код';

							// показываю ошибку
							var toastr = $injector.get('toastr');
							toastr.error('Неизвестная ошибка сервера', 'Ошибка: ' + httpCode);
						}
					}

					return response || $q.when(response);
				},
				'responseError': function(response) {
					var toastr = $injector.get('toastr');
					toastr.error('Неизвестная ошибка при выполнении действия!', 'Ошибка ' + response.status);

					return $q.reject(response);
				}
			};
		}
	]);
})(angular, nga);