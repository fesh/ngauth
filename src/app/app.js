(function(angular, nga) {
	'use strict';

	angular.module(nga.app.name, [
		nga.core.name
	]);
})(angular, nga);