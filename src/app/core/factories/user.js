(function(angular, nga) {
	'use strict';

	/**
	 * @ngdoc factory
	 * @name nga.core.factories.User
	 * @description Сущность пользователя.
	 */
	angular.module(nga.core.name).factory(nga.core.factories.User, [
		'toastr',
		'$injector',
		nga.core.factories.BaseModel,

		function(toastr, $injector, BaseModel) {
			function User(fields) {
				BaseModel.call(this);
				this.unserialize(fields);
			}

			User.prototype = new BaseModel();

			User.prototype.keyMap = {
				'id': 'id',
				'login': 'login',
				'email': 'email'
			};

			return User;
		}
	]);

	/**
	 * @ngdoc factory
	 * @name nga.core.factories.UserRole
	 * @description Сущность роли пользователя.
	 */
	angular.module(nga.core.name).factory(nga.core.factories.UserRole, [

		function() {
			return UserRole;
		}
	]);

	/**
	 * @ngdoc factory
	 * @name nga.core.factories.UserManager
	 * @description Менеджер для работы с пользователями посредством backend'а.
	 * */
	angular.module(nga.core.name).factory(nga.core.factories.UserManager, [
		'$http',
		'$q',
		'toastr',
		nga.core.constants.ApiConfig,
		nga.core.factories.BaseManager,
		nga.core.factories.User,

		function($http, $q, toastr, ApiConfig, BaseManager, User) {
			function UserManager() {
				BaseManager.call(this, User, 'UserHistory');

				this.controllerRoute = 'UserHistory';

				// Текущий пользователь.
				this.currentUser = null;
			}

			UserManager.prototype = new BaseManager();

			/**
			 * @ngdoc method
			 * @name checkSession
			 * @methodOf nga.core.factories.UserManager
			 * @description
			 * Отправка данных пользователя backend'у для регистрации в системе. В случае успешной
			 * регистрации заводится текущий пользователь в системе.
			 */
			UserManager.prototype.checkSession = function() {
				var self = this;

				// а нет никакой сессии в этом проекте
				return $q.when(false);
			};

			/**
			 * @ngdoc method
			 * @name register
			 * @methodOf nga.core.factories.UserManager
			 * @description
			 * Отправка данных пользователя backend'у для регистрации в системе. В случае успешной
			 * регистрации заводится текущий пользователь в системе.
			 */
			UserManager.prototype.register = function(fields) {
				var self = this;
				this.currentUser = null;

				// дополняю поля формы обязательными аттрибутами
				fields.request_type = 1;
				fields.submit = 1;

				return $http({
					url: ApiConfig.url + 'Profile/reg',
					method: 'POST',
					data: fields
				}).then(function(response) {
					self.currentUser = new User(response.data);
					return response.data;
				});
			};


			/**
			 * @ngdoc method
			 * @name captcha
			 * @methodOf nga.core.factories.UserManager
			 * @description
			 * Получение капчи для регистрации
			 */
			UserManager.prototype.captcha = function() {
				return $http({
					url: ApiConfig.url + 'Profile/captcha',
					method: 'POST',
					data: {
						request_type: 1
					}
				});
			};

			/**
			 * @ngdoc method
			 * @name authorize
			 * @methodOf nga.core.factories.UserManager
			 * @description Проверка прав доступа пользователя.
			 */
			UserManager.prototype.authorize = function(loginRequired, roles, checkType) {
				checkType = checkType || nga.core.enums.AccessCheckTypes.AtLeastOneRequired; // по умолчанию требуется одна из ролей
				if (loginRequired === true && this.currentUser === null) {
					return nga.core.enums.UserStatus.LoginRequired;
				} else if (loginRequired === true && this.currentUser !== null && (!roles || roles.length === 0)) {
					return nga.core.enums.UserStatus.Authorized;
				} else if (roles && this.currentUser !== null) {
					return this.currentUser.hasRole(roles, checkType) ? nga.core.enums.UserStatus.Authorized : nga.core.enums.UserStatus.Unauthorized;
				} else {
					return nga.core.enums.UserStatus.Authorized;
				}
			};

			return new UserManager();
		}
	]);
})(angular, nga);