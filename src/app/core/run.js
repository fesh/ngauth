(function(angular, nga) {
	'use strict';

	angular.module(nga.core.name).run([
		'$rootScope',
		'$state',
		'$templateCache',
		'amMoment',
		'angularMomentConfig',
		nga.core.constants.ApiConfig,
		nga.core.factories.UserManager,

		function($rootScope,
			$state,
			$templateCache,
			amMoment,
			angularMomentConfig,
			ApiConfig,
			UserManager) {

			// Вешаем обработчик на событие начала изменение состояния. Тут необходимо проверить,
			// требует ли новое состояние входа пользователя, а также обладает ли он достаточными
			// правами. Если требуется вход и он еще не был произведен, мы перекидываем пользователя
			// на страницу входа. Если пользователь не имеет соответствующих прав доступа, перекидываем
			// его на страницу уведомления об этом.
			$rootScope.$on('$stateChangeStart', function(event, toState, toParams) {

				if (toState.access && toState.access.loginRequired && toState.access.loginRequired !== false) {
					// Проверим, авторизован ли пользователь.
					var result = UserManager.authorize(toState.access.loginRequired, toState.access.roles, toState.access.checkType);

					if (result === nga.core.enums.UserStatus.LoginRequired) { // требуется вход
						// Запоминаем состояние, на которое собирались перейти.
						$state.previousState = toState;
						$state.previousStateParams = toParams;

						event.preventDefault();
						$state.go(nga.core.states.UserRegister);
					} else if (result === nga.core.enums.UserStatus.Unauthorized) { // нет доступа
						event.preventDefault();
						$state.go(nga.core.states.UserAccessDenied);
					}
				}
			});

			// Вешаем обработчик на событие успешного перехода к новому состоянию.
			$rootScope.$on('$stateChangeSuccess', function(event, toState) {
				// Если мы перешли на страницу входа, но пользователь уже вошел (из сессии),
				// перекидываем его сразу на нужную страницу.
				if (toState.name === nga.core.states.UserLogin && UserManager.currentUser !== null && $state.previousState && $state.previousState.name.length > 0) {
					$state.go($state.previousState.name, $state.previousStateParams);
				}
			});
		}
	]);
})(angular, nga);